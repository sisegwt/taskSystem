const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const mongoose = require('mongoose');

const controller = require('./controller');
const config = require('./config/config')
const log = require('./utils/log')


mongoose.connect(config.mongodb).then(
    () =>{
        log.level='debug';
        log.debug('mongodb is ready');
    },
    err =>{
        log.level='error'
        log.error('mongodb connect error:',err)
    }
);


const app = new Koa();
app.use(bodyParser());

//log URL
app.use(async (ctx,next)=>{
    console.log(`${ctx.request.method} ${ctx.request.url}`);
    await next();
})

app.use(controller());

app.listen(3000);
console.log('app started at port 3000...');