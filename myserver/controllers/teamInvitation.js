const teamInvitation = require('../models/teamInvitation')
const teamWork = require('../models/teamWork')

/**
 * 
 * 发送邀请
 * inviter（邀请人ID），invitee（被邀请人ID），teamName(团队名字)，course（团队所在班级）
 * return success or error（邀请已发送）
 * 
 */
var sendInvitation = async(ctx,next)=>{
    let document = await teamInvitation.findOne({inviter:ctx.request.body.inviter,
        invitee:ctx.request.body.invitee,teamName:ctx.request.body.teamName,course:ctx.request.body.course});
    if(document){
        return ctx.response.body = {stauts:'error',msg:'邀请已发送'};
    }

    var teamInvitationEntity = new teamInvitation({
        inviter:ctx.request.body.inviter,
        invitee:ctx.request.body.invitee,
        teamName:ctx.request.body.teamName,
        course:ctx.request.body.course,
        status:true
    })

    await teamInvitationEntity.save(function(err,doc){
        if(err){
            console.log(err);
        }else{
            console.log(doc);
        }
    })
    return ctx.response.body = {stauts:'success',msg:'成功',teamInvitationEntity};
}

/**
 * 
 * 接受邀请
 * inviter（邀请人ID），invitee（被邀请人ID），teamName(团队名字)，course（团队所在班级）
 * return success or error（被邀请人在其他团队里面）
 * 
 */
var acceptInvitation = async(ctx,next)=>{
    let document = await teamWork.findOne({course:ctx.request.body.course,teamMember:ctx.request.body.invitee});
    if(document){
        return ctx.response.body = {stauts:'error',msg:'你已有团队'};
    }
    var team = await teamWork.findOne({teamName:ctx.request.body.teamName,course:ctx.request.body.course});
    await teamWork.findByIdAndUpdate(team._id,{$push:{teamMember:ctx.request.body.invitee}})
    var invitation = await teamInvitation.findOne({inviter:ctx.request.body.inviter,invitee:ctx.request.body.invitee,teamName:ctx.request.body.teamName,course:ctx.request.body.course})
    await teamInvitation.findByIdAndRemove(invitation._id);
    return ctx.response.body = {stauts:'success',msg:'成功'};
}

/**
 * 
 * 拒绝邀请
 * inviter（邀请人ID），invitee（被邀请人ID），teamName(团队名字)，course（团队所在班级）
 * return success or error（被邀请人在其他团队里面）
 * 
 */
var refuseInvitation = async(ctx,next)=>{
    var invitation = await teamInvitation.findOne({inviter:ctx.request.body.inviter,invitee:ctx.request.body.invitee,teamName:ctx.request.body.teamName,course:ctx.request.body.course})
    console.log(invitation)
    await teamInvitation.findByIdAndRemove(invitation._id);
    return ctx.response.body = {stauts:'success',msg:'成功'};
}
/**
 * 
 * 红点提示修改
 * inviter（邀请人ID），invitee（被邀请人ID），teamName(团队名字)，course（团队所在班级）
 * 
 */
var setInvitation = async(ctx,next)=>{
    let document = await teamInvitation.findOne({inviter:ctx.request.body.inviter,invitee:ctx.request.body.invitee,teamName:ctx.request.body.teamName,course:ctx.request.body.course});
    await teamInvitation.findByIdAndUpdate(document._id,{$set:{status:false}})
    return ctx.response.body = {stauts:'success',msg:'成功'};
}
module.exports = {
    'POST /teamInvitation/sendInvitation': sendInvitation,
    'POST /teamInvitation/acceptInvitation': acceptInvitation,
    'POST /teamInvitation/setInvitation': setInvitation,
    'POST /teamInvitation/refuseInvitation': refuseInvitation,
}