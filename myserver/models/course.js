const mongoose = require('mongoose');

let Schema = mongoose.Schema

let courseSchema = new Schema({
    course:String,
    teacher:String,
    student:{type:Array,default:[]},
    time:{type:String,default:""},
    week:{type:String,default:""},
    place:{type:String,default:""},
    isTeam:{type:Boolean,default:false}
})

module.exports = mongoose.model('course',courseSchema)