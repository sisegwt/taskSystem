const mongoose = require('mongoose');

let Schema = mongoose.Schema

let userSchema = new Schema({
    userID:String,
    password:String,
    userName:String,
    root:{type:Number,default:4},//1:管理员，2：老师，3：管理员老师，4：学生
    course:{type:Array,default:[]},
    major:String,
    contact:String
})

module.exports = mongoose.model('user',userSchema)