const mongoose = require('mongoose');

let Schema = mongoose.Schema

let teamInvitationSchema = new Schema({
    inviter:String,//邀请人
    invitee:String,//被邀请人
    teamName:String,
    course:String,
    status:Boolean//用于记录红点提示状态，true为有红点，false为无红点
})

module.exports = mongoose.model('teamInvitation',teamInvitationSchema)