const mongoose = require('mongoose');

let Schema = mongoose.Schema

let publishSchema = new Schema({
    workName:String,
    course:String,
    startTime:{type:Date,default:Date.now()},
    endTime:{type:Date,default:""},
    workDetails:{type:String,default:""},
    accessory:{type:String,default:""},//文件路径
    isTeam:{type:String,default:""},//1为个人作业  2为团队作业
})

module.exports = mongoose.model('publish',publishSchema)