const mongoose = require('mongoose');

let Schema = mongoose.Schema

let teamWorkSchema = new Schema({
    teamName:String,
    teamMember:Array,
    work:{type:Array,default:[]},
    course:String,
})

module.exports = mongoose.model('teamWork',teamWorkSchema)