const mongoose = require('mongoose');

let Schema = mongoose.Schema

let studentWorkSchema = new Schema({
    studentID:String,
    name:String,
    workName:String,
    course:String,
    startTime:{type:Date,default:""},
    endTime:{type:Date,default:""},
    accessory:{type:String,default:""},//文件路径
    workStatus:{type:Number,default:1},//记录是否提交的状态  -1为迟交（黄）  0为未交（红）  1为正常（黑） 2为已交（绿）
    score:{type:String,default:""},//分数
    submitTime:{type:Date,default:""},//提交时间
    evaluate:{type:String,default:""}//评价
})

module.exports = mongoose.model('studentWork',studentWorkSchema)