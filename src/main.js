// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from "vue-router"
import routers from './router'
import store from './vuex/store'
import ElementUI from 'element-ui'
import 'element-ui/lib/gray/index.css'
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(VueRouter);
/* eslint-disable no-new */

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: routers
})

new Vue({
  el: '#app',
  router:router,
  store,
  template: '<App/>',
  components: { App }
})
