import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Manage/Home'
import uTeacher from '@/components/Manage/uTeacher'
import uStudent from '@/components/Manage/uStudent'
import Edit from '@/components/Manage/Edit'
import Class_M from '@/components/Manage/Class_M'
import PushHistory from '@/components/Manage/PushHistory'
import PushDetails from '@/components/Manage/PushDetails'
import CommitHistory from '@/components/Manage/CommitHistory'
import CommitDetails from '@/components/Manage/CommitDetails'
import login from '@/components/login'
import teacher from '@/components/teacher'
import student from '@/components/student'
import tIndv from '@/components/tIndv.vue'
import tTeam from '@/components/tTeam.vue'
import sIndv from '@/components/sIndv.vue'
import sTeam from '@/components/sTeam.vue'
import publish from '@/components/publish.vue'
import addClass from '@/components/addClass.vue'
import score from '@/components/score.vue'
import changepassword from '@/components/changepassword.vue'
Vue.use(Router)
Vue.component('tIndv',tIndv)
Vue.component('tTeam',tTeam)
Vue.component('sIndv',sIndv)
Vue.component('sTeam',sTeam)
Vue.component('publish',publish)
Vue.component('addClass',addClass)
Vue.component('score',score)

var router= [
    {
      path: '/Home',
      name: 'root',
      component: Home,
      redirect: '/uTeacher',
      children: [
        { path: '/uTeacher',
          component: uTeacher,
        },
        { path: '/uStudent',
          component: uStudent,
        },
        { path: '/Edit',
          component: Edit },
        { path: '/Class_M',
          component: Class_M },
        { path: '/PushHistory',
          component: PushHistory,
        },
        { path: '/PushDetails',
          component: PushDetails,
        },
        { path: '/CommitHistory',
          component: CommitHistory,
        },
        { path: '/CommitDetails',
          component: CommitDetails,
        },
        // { path: 'Search',
        //   component: Search ,
        // },
      ]
    },

     {
      path: '/',
      component: login,
     },
     {
      path: '/teacher',
      component: teacher,
     },
      {
          path:'/tIndv',
          component:tIndv
      },
      {
          path:'/tTeam',
          component:tTeam
      },
      {
          path:'/sIndv',
          component:sIndv
      },
      {
          path:'/sTeam',
          component:sTeam
      },
     {
      path: '/publish',
      component: publish,
     },
     {
      path: '/addClass',
      component: addClass,
     },
     {
       path:'/student',
       component:student,
     },
    {
        path:'/score',
        component:score
    },
    {
      path:'/changepassword',
      component:changepassword
    }

  ]
  export default router
